package main

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Objeto struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender       string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_Type string `json:"vaccine_type"`
	Way          string `json:"way"`
}

func pullMsgs(channelName string) error {

	clientRedis := redis.NewClient(&redis.Options{
		Addr:     "104.154.85.192:6379",
		Password: "",
		DB:       0,
	})

	err := clientRedis.Ping(context.Background()).Err()

	if err != nil {
		time.Sleep(3 * time.Second)
		err := clientRedis.Ping(context.Background()).Err()
		if err != nil {
			panic(err)
		}
	}

	ctx := context.Background()

	topic := clientRedis.Subscribe(ctx, channelName)
	// Se obtiene el canal a utilizar
	channel := topic.Channel()
	// Iterar los mensajes enviados en el canal.
	for msg := range channel {
		//responseBody := bytes.NewBuffer([]byte(msg.Payload))
		clientRedis.LPush(ctx, "lista", msg.Payload)

		ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
		client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://35.238.21.124:27017"))
		if err != nil {
			panic(err)
		}
		//defer client.Disconnect(ctx)
		collection := client.Database("proyecto2").Collection("covid")

		u := &Objeto{}
		errorUsuario := u.UnmarshalBinary([]byte(msg.Payload))
		if err != nil {
			panic(errorUsuario)
		}

		_, errorInsercion := collection.InsertOne(ctx, u)
		if errorInsercion != nil {
			panic(errorInsercion)
		}

	}

	return nil
}

// UnmarshalBinary decodes the struct into a Objeto
func (u *Objeto) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, u); err != nil {
		return err
	}
	return nil
}

func (u *Objeto) MarshalBinary() ([]byte, error) {
	return json.Marshal(u)
}

/*
func (u *Objeto) String() string {
	return "User: " + u.Name
}


func handler(msg []byte) error {
	responseBody := bytes.NewBuffer([]byte(msg))
	resp, err := http.Post("http://35.223.145.130/api/casos", "application/json", responseBody)
	//Handle Error
	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}
	defer resp.Body.Close()
	// Leer la respuesta de la petición.

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}
	sb := string(body)
	log.Printf(sb)

	return nil
}
*/

func main() {
	fmt.Println("Subscriber Service Running...")
	pullMsgs("canal")
}
