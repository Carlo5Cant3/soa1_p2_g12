package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
)

func publish(channel string, msg Objeto) error {
	//fmt.Println(msg)
	client := redis.NewClient(&redis.Options{
		Addr:     "104.154.85.192:6379",
		Password: "",
		DB:       0,
	})

	err := client.Ping(context.Background()).Err()

	if err != nil {
		time.Sleep(3 * time.Second)
		err := client.Ping(context.Background()).Err()
		if err != nil {
			panic(err)
		}
	}

	ctx := context.Background()

	client.Publish(ctx, channel, &msg)

	// Sleep random time
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(4)
	time.Sleep(time.Duration(n) * time.Second)

	return nil
}

type Objeto struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender       string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_Type string `json:"vaccine_type"`
	Way          string `json:"way"`
}

// MarshalBinary codifica la estructura en un binary blob
func (u *Objeto) MarshalBinary() ([]byte, error) {
	return json.Marshal(u)
}

// UnmarshalBinary decodifica la estructura en un Objeto
func (u *Objeto) UnmarshalBinary(data []byte) error {
	if err := json.Unmarshal(data, &u); err != nil {
		return err
	}
	return nil
}

func enableCors(w *http.ResponseWriter) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
}

func http_server(w http.ResponseWriter, r *http.Request) {
	enableCors(&w)
	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("200 - OK!"))

	case "POST":
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}

		var o Objeto
		err := json.NewDecoder(r.Body).Decode(&o)
		if err != nil {
			http.Error(w, "404 not found.", http.StatusNotFound)
			return
		}
		o.Way = "Redis Pub/Sub"
		/*
			//message, err := json.Marshal(Objeto{Name: o.Name, Location: o.Location, Gender: o.Gender, Age: o.Age, Vaccine_Type: o.Vaccine_Type, Way: "Pub/Sub"})
			message, err := json.Marshal(Objeto{Name: o.Name, Location: o.Location, Gender: o.Gender, Age: o.Age, Vaccine_Type: o.Vaccine_Type, Way: "Pub/Sub"})

			if err != nil {
				fmt.Fprintf(w, "ParseForm() err: %v", err)
				return
			}

			fmt.Fprintf(w, "¡Mensaje Publicado!\n")
			fmt.Fprintf(w, "Name = %s\n", o.Name)
			fmt.Fprintf(w, "Location = %s\n", o.Location)
			fmt.Fprintf(w, "Gender = %s\n", o.Gender)
			fmt.Fprintf(w, "Age = %d\n", o.Age)
			fmt.Fprintf(w, "Vaccine_Type = %s\n", o.Vaccine_Type)
			fmt.Fprintln(w, string(message))
		*/

		//msg := o.Name + "," + o.Location + "," + o.Gender + "," + strconv.Itoa(o.Age) + "," + o.Vaccine_Type

		publish("canal", o)

	default:
		fmt.Fprintf(w, "Metodo %s no soportado \n", r.Method)
		return
	}
}

func salud(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "OK")
}

func main() {
	fmt.Println("Hello World From Publisher!")

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", http_server)
	router.HandleFunc("/salud", salud)
	http_port := ":8080"
	fmt.Printf("Running Server Google PubSub on http://localhost:80\n")

	if err := http.ListenAndServe(http_port, router); err != nil {
		log.Fatal(err)
	}
}
