module servidor.com/main

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible // indirect
	github.com/go-redis/redis/v8 v8.8.2 // indirect
	go.mongodb.org/mongo-driver v1.5.2 // indirect
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210510120150-4163338589ed // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/text v0.3.6 // indirect
	google.golang.org/grpc v1.36.0 // indirect
	google.golang.org/grpc/examples v0.0.0-20210323221903-faf4e1c777f0 // indirect
)
