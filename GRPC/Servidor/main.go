package main

import (
	"context"
	"encoding/json"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	pb "google.golang.org/grpc/examples/helloworld/helloworld"

	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Objeto struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender       string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_Type string `json:"vaccine_type"`
	Way          string `json:"way"`
}

const (
	port = ":4000"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

// server is used to implement helloworld.GreeterServer.
type server struct {
	pb.UnimplementedGreeterServer
}

// SayHello implements helloworld.GreeterServer
func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Received: %v", in.GetName())

	clientRedis := redis.NewClient(&redis.Options{
		Addr:     "104.154.85.192:6379",
		Password: "",
		DB:       0,
	})

	err := clientRedis.Ping(context.Background()).Err()

	if err != nil {
		time.Sleep(3 * time.Second)
		err := clientRedis.Ping(context.Background()).Err()
		if err != nil {
			panic(err)
		}
	}

	ctx = context.Background()

	clientRedis.LPush(ctx, "lista", in.GetName())

	clientOptions := options.Client().ApplyURI("mongodb://35.238.21.124:27017")

	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		panic(err)
	}

	collection := client.Database("proyecto2").Collection("covid")

	var u Objeto
	err = json.Unmarshal([]byte(string(in.GetName())), &u)
	if err != nil {
		panic(err)
	}

	_, err = collection.InsertOne(context.TODO(), u)
	if err != nil {
		panic(err)
	}

	return &pb.HelloReply{Message: "si funciona"}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
