const express = require('express');
const app = express();

//BD
const { mongose } = require('./database');


//Configuraciones
app.set('port', process.env.PORT || 5000);

//Middlewares
app.use(express.json());

//Rutas
app.use('/', require('./routes/routs'));

app.listen(app.get('port'), () =>{
    console.log(`Servidor en puerto ${app.get('port')}`);
})