const express = require('express');
const router = express.Router();
const Covid = require('../models/covid');

var redis = require('redis');
const { count } = require('../models/covid');
var client = redis.createClient('6379', '104.154.85.192');

client.on('connect', function() {
    console.log('Redis connected');
});

//Datos almacenados en la base de datos, en MongoDB.
router.get('/mongo/', async (req, res) => {
    const VacCovid = await Covid.find();
    res.json(VacCovid);
});

//Los diez países con más vacunados, en Redis.
router.get('/redis/top10', async (req, res) => {
    client.lrange('lista', 0, -1, function(err, reply) {

        var lista = [];

        for(dato in reply){
            lista.push(JSON.parse(reply[dato]))
        }

        let Conteo = [
            {
                location: "Alta Verapaz",
                count: lista.filter(o => o.location == "Alta Verapaz").length
            },
            {
                location: "Baja Verapaz",
                count: lista.filter(o => o.location == "Baja Verapaz").length
            },
            {
                location: "Chimaltenango",
                count: lista.filter(o => o.location == "Chimaltenango").length
            },
            {
                location: "Chiquimula",
                count: lista.filter(o => o.location == "Chiquimula").length
            },
            {
                location: "El Progreso",
                count: lista.filter(o => o.location == "El Progreso").length
            },
            {
                location: "Escuintla",
                count: lista.filter(o => o.location == "Escuintla").length
            },
            {
                location: "Guatemala",
                count: lista.filter(o => o.location == "Guatemala").length
            },
            {
                location: "Huehuetenango",
                count: lista.filter(o => o.location == "Huehuetenango").length
            },
            {
                location: "Izabal",
                count: lista.filter(o => o.location == "Izabal").length
            },
            {
                location: "Jalapa",
                count: lista.filter(o => o.location == "Jalapa").length
            },
            {
                location: "Jutiapa",
                count: lista.filter(o => o.location == "Jutiapa").length
            },
            {
                location: "Petén",
                count: lista.filter(o => o.location == "Petén").length
            },
            {
                location: "Quetzaltenango",
                count: lista.filter(o => o.location == "Quetzaltenango").length
            },
            {
                location: "Quiché",
                count: lista.filter(o => o.location == "Quiché").length
            },
            {
                location: "Retalhuleu",
                count: lista.filter(o => o.location == "Retalhuleu").length
            },
            {
                location: "Sacatepéquez",
                count: lista.filter(o => o.location == "Sacatepéquez").length
            },
            {
                location: "San Marcos",
                count: lista.filter(o => o.location == "San Marcos").length
            },
            {
                location: "Santa Rosa",
                count: lista.filter(o => o.location == "Santa Rosa").length
            },
            {
                location: "Sololá",
                count: lista.filter(o => o.location == "Sololá").length
            },
            {
                location: "Suchitepéquez",
                count: lista.filter(o => o.location == "Suchitepéquez").length
            },
            {
                location: "Totonicapán",
                count: lista.filter(o => o.location == "Totonicapán").length
            },
            {
                location: "Zacapa",
                count: lista.filter(o => o.location == "Zacapa").length
            }
        ];

        Conteo.sort((a, b) => b.count - a.count)

        res.json(Conteo);
    });

});

//Personas vacunadas por cada país, en Redis.
router.post('/redis/personas', async (req, res) => {
    client.lrange('lista', 0, -1, function(err, reply) {
        var lista = [];

        for(dato in reply){
            if (JSON.parse(reply[dato]).location == req.body.location){
                lista.push(JSON.parse(reply[dato]))
            }
        }
        res.json(lista);
    });
});

//Gráfica de pie de los géneros de los vacunados por país, en MongoDB.
router.post('/mongo/generos', async (req, res) => {
    const CasosCovid = await Covid.find();

    var lista = [];

    for(dato in CasosCovid){
        if (CasosCovid[dato].location == req.body.location){
            lista.push(CasosCovid[dato]);
        }
    }
    
    let Conteo = [
        {
            genero: "male",
            count: lista.filter(o => o.gender == "male").length
        },
        {
            genero: "female",
            count: lista.filter(o => o.gender == "female").length
        }
    ];

    res.json(Conteo);
});

//Los últimos cinco vacunados almacenados por país, en MongoDB.
router.post('/mongo/ultimos', async (req, res) => {
    const CasosCovid = await Covid.find();
    var lista = [];

    for(dato in CasosCovid){
        if (CasosCovid[dato].location == req.body.location){
            lista.push(CasosCovid[dato]);
        }
    }

    console.log(lista);

    if(lista.length >= 5){
        res.json(lista.slice(lista.length-5));
    }else{
        res.json(lista);
    }
    
});

//Gráfica de barras del rango de edades (de diez en diez) por cada país, en Redis.
router.post('/redis/edades', async (req, res) => {
    client.lrange('lista', 0, -1, function(err, reply) {
        var lista = [];

        for(dato in reply){
            if (JSON.parse(reply[dato]).location == req.body.location){
                lista.push(JSON.parse(reply[dato]))
            }
        }

        var Conteo = [
            {
                rango: "0-10",
                count: lista.filter(o => o.age > 0 && o.age <= 10).length
            },
            {
                rango: "11-20",
                count: lista.filter(o => o.age > 10 && o.age <= 20).length
            },
            {
                rango: "21-30",
                count: lista.filter(o => o.age > 20 && o.age <= 30).length
            },
            {
                rango: "31-40",
                count: lista.filter(o => o.age > 30 && o.age <= 40).length
            },
            {
                rango: "41-50",
                count: lista.filter(o => o.age > 40 && o.age <= 50).length
            },
            {
                rango: "51-60",
                count: lista.filter(o => o.age > 50 && o.age <= 60).length
            },
            {
                rango: "61-70",
                count: lista.filter(o => o.age > 60 && o.age <= 70).length
            },
            {
                rango: "71-80",
                count: lista.filter(o => o.age > 70 && o.age <= 80).length
            },
            {
                rango: "81-90",
                count: lista.filter(o => o.age > 80 && o.age <= 90).length
            },
            {
                rango: "91-100",
                count: lista.filter(o => o.age > 90 && o.age <= 100).length
            }
        ]

        res.json(Conteo);
    });
});

module.exports = router;