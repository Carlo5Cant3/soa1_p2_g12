const mongoose = require('mongoose');

const schema = mongoose.Schema({
    name : String,
    location : String,
    gender: String,
    age : Number,
    vaccine_type : String,
    way : String
},{ collection : 'covid' });

module.exports = mongoose.model('covid',schema);