const mongoose = require('mongoose');
const URI = 'mongodb://35.238.21.124:27017/proyecto2';


mongoose.connect(URI)
        .then(db => console.log('Base de datos conectada'))
        .catch(error => console.error(error));

module.exports = mongoose;