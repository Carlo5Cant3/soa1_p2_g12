import React from 'react';
import { useParams } from 'react-router';
import './Mapa.css'


function Mapa() {
    let { id } = useParams();

    return(
        <div className="Mapa">
            <h1>{id}</h1>
        </div>
    );
}



export default Mapa;