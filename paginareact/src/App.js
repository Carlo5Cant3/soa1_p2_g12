import React from 'react';
import './App.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import Mapa from "./mapa/Mapa";
import TablaMongo from "./reportes/TablaMongo";
import DepaRedis from "./reportes/DepaRedis";
import PersonasDepaRedis from "./reportes/PersonasDepaRedis";
import PieGenerosMongo from "./reportes/PieGenerosMongo";
import BarrasEdadesRedis from "./reportes/BarrasEdadesRedis";

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="navbar navbar-dark bg-dark">
          <div className="container-fluid">
            <a className="navbar-brand" href="/">Grupo 12</a>
            <a className="navbar-brand" href="/DatosMongo">Datos MongoDB</a>
            <a className="navbar-brand" href="/DepaRedis">Departamentos mas vacunados Redis</a>
          </div>
        </nav>
        <Route path="/DatosMongo" component={TablaMongo}></Route>
        <Route path="/DepaRedis" component={DepaRedis}></Route>
        <Route path="/Mapa/:id" component={Mapa}></Route>
        <Route path="/Mapa/:id" component={PersonasDepaRedis}></Route>
        <Route path="/Mapa/:id" component={PieGenerosMongo}></Route>
        <Route path="/Mapa/:id" component={BarrasEdadesRedis}></Route>

      </div>
    </Router>
  );
}

export default App;
