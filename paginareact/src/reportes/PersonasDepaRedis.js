import React, { useState, useEffect } from "react";
import axios from 'axios';
import MaterialTable from 'material-table';
import { useParams } from 'react-router';

var depa;

function PersonasDepaRedis() {
    let { id } = useParams();
    depa = id;
    const [Datos, setDatos] = useState([]);

    const columnas = [
        {
          title: 'Nombre',
          field: 'name',
          filtering: false
        },
        {
          title: 'Edad',
          field: 'age',
          filtering: false
        },
        {
          title: 'Genero',
          field: 'gender',
          filtering: false
        },
        {
          title: 'Tipo de Vacuna',
          field: 'vaccine_type',
          filtering: false
        },
        {
            title: 'Departamento',
            field: 'location',
            filtering: false
        },
        {
          title: 'Ruta',
          field: 'way',
          lookup: {'GRPC':'GRPC', 'Redis Pub/Sub':'Redis Pub/Sub'}
        }
    ]

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios({
                method: 'post',
                url: "https://api-mongo-redis.herokuapp.com/redis/personas",
                data: {
                    location: depa
                }
            });
            setDatos(response.data);
    
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchData();
    }, [setDatos]);

    return(
        <div className="PersonasDepaRedis">
            <MaterialTable 
            columns = {columnas}
            data = {Datos}
            title = "Personas vacunadas (Redis)"
          />
        </div>
    );

}

export default PersonasDepaRedis;