import React, { useState, useEffect } from "react";
import axios from 'axios';
import {CanvasJSChart} from 'canvasjs-react-charts'
import { useParams } from 'react-router';

var depa;

function BarrasEdadesRedis() {
    let { id } = useParams();
    depa = id;

    const [options, setOptions] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios({
                method: 'post',
                url: "https://api-mongo-redis.herokuapp.com/redis/edades",
                data: {
                    location: depa
                }
            });
            
            const options = {
                title: {
                    text: "Vacunados por rangos de edades"
                },
                data: [
                {
                    // Change type to "doughnut", "line", "splineArea", etc.
                    type: "column",
                    dataPoints: [
                        { label: response.data[0].rango,  y: response.data[0].count  },
                        { label: response.data[1].rango,  y: response.data[1].count  },
                        { label: response.data[2].rango,  y: response.data[2].count  },
                        { label: response.data[3].rango,  y: response.data[3].count  },
                        { label: response.data[4].rango,  y: response.data[4].count  },
                        { label: response.data[5].rango,  y: response.data[5].count  },
                        { label: response.data[6].rango,  y: response.data[6].count  },
                        { label: response.data[7].rango,  y: response.data[7].count  },
                        { label: response.data[8].rango,  y: response.data[8].count  },
                        { label: response.data[9].rango,  y: response.data[9].count  },
                    ]
                }
                ]
            }

              setOptions(options);
    
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchData();
      }, [setOptions]);

    return (
        <div className="BarrasEdadesRedis">
             <CanvasJSChart options = {options}/>
        </div>
    );
}

export default BarrasEdadesRedis;