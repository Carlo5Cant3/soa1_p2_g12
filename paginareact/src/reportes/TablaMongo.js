import React, { useState, useEffect } from "react";
import axios from 'axios';
import MaterialTable from 'material-table';


function TablaMongo() {
    const [Datos, setDatos] = useState([]);

    const columnas = [
        {
          title: 'Nombre',
          field: 'name',
          filtering: false
        },
        {
          title: 'Edad',
          field: 'age',
          filtering: false
        },
        {
          title: 'Genero',
          field: 'gender',
          filtering: false
        },
        {
          title: 'Tipo de Vacuna',
          field: 'vaccine_type',
          filtering: false
        },
        {
            title: 'Departamento',
            field: 'location',
            filtering: false
        },
        {
          title: 'Ruta',
          field: 'way',
          lookup: {'GRPC':'GRPC', 'Redis Pub/Sub':'Redis Pub/Sub', 'KAFKA':'KAFKA'}
        }
    ]

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios({
              url: "https://api-mongo-redis.herokuapp.com/mongo",
            });
    
            setDatos(response.data);
    
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchData();
    }, [setDatos]);

    return(
        <div className="TablaMongo">
            <MaterialTable 
            columns = {columnas}
            data = {Datos}
            title = "Vacunados de COVID (MongoDB)"
          />
        </div>
    );

}

export default TablaMongo;