import React, { useState, useEffect } from "react";
import axios from 'axios';
import {CanvasJSChart} from 'canvasjs-react-charts'
import { useParams } from 'react-router';

var depa;

function PieGenerosMongo() {
    let { id } = useParams();
    depa = id;

    const [options, setOptions] = useState([]);
    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios({
                method: 'post',
                url: "https://api-mongo-redis.herokuapp.com/mongo/generos",
                data: {
                    location: depa
                }
            });
            

            const porc0 = (response.data[0].count * 100) / (response.data[0].count + response.data[1].count);
            const porc1 = (response.data[1].count * 100) / (response.data[0].count + response.data[1].count);
            

            const options = {
                exportEnabled: true,
                animationEnabled: true,
                title: {
                  text: "Generos mas vacunados (MongoDB)"
                },
                data: [{
                  type: "pie",
                  startAngle: 75,
                  toolTipContent: "<b>{label}</b>: {y}%",
                  showInLegend: "true",
                  legendText: "{label}",
                  indexLabelFontSize: 16,
                  indexLabel: "{label} - {y}%",
                  dataPoints: [
                    { y: porc0.toFixed(2), label: "Hombres - " + response.data[0].count },
                    { y: porc1.toFixed(2), label: "Mujeres - " + response.data[1].count }
                  ]
                }]
              }

              setOptions(options);
    
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchData();
      }, [setOptions]);

      return (
        <div className="PieGenerosMongo">
             <CanvasJSChart options = {options}/>
        </div>
    );
}


export default PieGenerosMongo;