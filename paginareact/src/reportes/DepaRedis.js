import React, { useState, useEffect } from "react";
import axios from 'axios';
import MaterialTable from 'material-table';


function DepaRedis() {
    const [Datos, setDatos] = useState([]);

    const columnas = [
        {
          title: 'Departamento',
          field: 'location',
          filtering: false
        },
        {
          title: 'Cantidad de vacunados',
          field: 'count',
          filtering: false
        }
    ]

    useEffect(() => {
        const fetchData = async () => {
          try {
            const response = await axios({
              url: "https://api-mongo-redis.herokuapp.com/redis/top10",
            });
    
            setDatos(response.data);
    
          } catch (error) {
            console.log(error);
          }
        };
    
        fetchData();
    }, [setDatos]);

    return(
        <div className="DepaRedis">
            <MaterialTable 
            columns = {columnas}
            data = {Datos}
            title = "Departamentos mas vacunados (Redis)"
          />
        </div>
    );

}

export default DepaRedis;