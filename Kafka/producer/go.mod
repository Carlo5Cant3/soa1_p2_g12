module producer.go

go 1.16

require (
	github.com/Shopify/sarama v1.29.0 // indirect
	github.com/gin-gonic/gin v1.7.1 // indirect
	github.com/moemoe89/simple-kafka-golang/producer v0.0.0-20200306042424-78f10add423c // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1 // indirect
)
