package main

import (
	conf "/config"

	"encoding/json"
	"log"
	"time"
	"context"

	"github.com/Shopify/sarama"

	"github.com/go-redis/redis/v8"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)


type Objeto struct {
	Name         string `json:"name"`
	Location     string `json:"location"`
	Gender       string `json:"gender"`
	Age          int    `json:"age"`
	Vaccine_Type string `json:"vaccine_type"`
	Way          string `json:"way"`
}


const (
	KAFKA_TOPIC = "simple-kafka-golang"
)

func main(){
	consumer, err := conf.InitKafkaConsumer()
	if err != nil {
		panic(err)
	}

	partitionConsumer, err := consumer.ConsumePartition(KAFKA_TOPIC, 0, sarama.OffsetNewest)
	if err != nil {
		panic(err)
	}

	log.Print("Connected to kafka broker")

	for m := range partitionConsumer.Messages() {

		//log.Print(&user)
		//log.Print("raw : ",text)
		//log.Print("user id : ",user.ID)
		//log.Print("name : ",user.Name)

		clientRedis := redis.NewClient(&redis.Options{
			Addr:     "104.154.85.192:6379",
			Password: "",
			DB:       0,
		})
	
		err = clientRedis.Ping(context.Background()).Err()
	
		if err != nil {
			time.Sleep(3 * time.Second)
			err := clientRedis.Ping(context.Background()).Err()
			if err != nil {
				panic(err)
			}
		}
	
		ctx := context.Background()
	
		clientRedis.LPush(ctx, "lista", m.Value)



		clientOptions := options.Client().ApplyURI("mongodb://35.238.21.124:27017")

		client, err := mongo.Connect(context.TODO(), clientOptions)
		if err != nil {
			panic(err)
		}

		err = client.Ping(context.TODO(), nil)
		if err != nil {
			panic(err)
		}

		collection := client.Database("proyecto2").Collection("covid")

		text := string(m.Value)
		bytes := []byte(text)
		log.Print(text)

		var caso Objeto

		//var user caso
		err = json.Unmarshal(bytes, &caso)
		if err != nil {
			panic(err)
		}

		_, err = collection.InsertOne(context.TODO(), caso)
		if err != nil {
			panic(err)
		}

	}

}