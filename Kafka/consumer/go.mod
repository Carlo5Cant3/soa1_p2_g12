module consumer.go

go 1.16

require (
	github.com/Shopify/sarama v1.29.0 // indirect
	github.com/go-redis/redis/v8 v8.8.3 // indirect
	github.com/moemoe89/simple-kafka-golang/consumer v0.0.0-20200306042424-78f10add423c // indirect
	go.mongodb.org/mongo-driver v1.5.2 // indirect
)
